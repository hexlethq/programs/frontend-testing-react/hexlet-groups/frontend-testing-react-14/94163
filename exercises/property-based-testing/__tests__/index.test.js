const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
test('should correct sort arrays', () => {
  fc.assert(
    fc.property(
      fc.array(fc.nat()),
      fc.array(fc.string()),
      (numberList, stringList) => {
        expect(sort(numberList)).toBeSorted();
        expect(sort(stringList)).toBeSorted({ coerce: true });
      },
    ),
  );
});
test('should correct sort descending', () => {
  fc.assert(
    fc.property(
      fc.array(fc.nat()),
      fc.array(fc.string()),
      (numberList, stringList) => {
        expect(sort(numberList).reverse()).toBeSorted({ descending: true });
        expect(sort(stringList).reverse()).toBeSorted({ descending: true, coerce: true });
      },
    ),
  );
});
test('should correct work with double sorting', () => {
  fc.assert(
    fc.property(
      fc.array(fc.nat()),
      fc.array(fc.string()),
      (numberList, stringList) => {
        expect(sort(sort(numberList))).toBeSorted();
        expect(sort(sort(stringList))).toBeSorted({ coerce: true });
      },
    ),
  );
});
// END
