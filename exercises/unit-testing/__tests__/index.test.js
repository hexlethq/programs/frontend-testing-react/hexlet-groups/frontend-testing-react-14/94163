test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  // BEGIN
  const expected = { k: 'v', a: 'a', b: 'b' };
  expect(result).toEqual(expected);
  // END
});

test('should correct work with multiple targets', () => {
  const src = { k: 'v', b: 'b' };
  const target = { a: 'a', c: 'c' };
  const target2 = { d: 'd', e: 'e' };
  const result = Object.assign(src, target, target2);

  const expected = {
    k: 'v',
    b: 'b',
    a: 'a',
    c: 'c',
    d: 'd',
    e: 'e',
  };
  expect(result).toEqual(expected);
});

test('should correct work with empty target', () => {
  const src = { k: 'v', b: 'b' };
  const target = {};
  const result = Object.assign(src, target);

  const expected = { k: 'v', b: 'b' };
  expect(result).toEqual(expected);
});

test('should correct clone object', () => {
  const src = {};
  const target = { k: 'v', b: 'b' };
  const result = Object.assign(src, target);

  target.b = 'a';

  const expected = { k: 'v', b: 'b' };
  expect(result).toEqual(expected);
});

test('should correct works with readOnly props', () => {
  const src = { k: 'v', b: 'b' };
  Object.defineProperty(src, 'k', {
    value: 'v',
    writable: false,
  });

  const target = { k: 't' };
  expect(() => Object.assign(src, target)).toThrow(TypeError);
});

test('should return src object', () => {
  const src = { k: 'v', b: 'b' };
  const target = { d: 'd' };
  const result = Object.assign(src, target);
  expect(result).toBe(src);
});
